const express = require("express");

const router = express.Router(); //use .Router if it is not the main app
const checkAuth = require('../middleware/check-auth');
const checkAdmin = require('../middleware/check-admin');
//get the Fruit model
const PropertyModel = require("../models/Property");


//============CREATE A NEW FRUIT================//
router.post("/create", function (req, res) {
    // let newFruit = FruitModel({
    // 	"name": "kiwi",
    // 	"color": "green",
    // 	"price": 20,
    // 	"taste": "sour"
    // });

    // res.send(req.body);

    let newProperty = PropertyModel({
        "name": req.body.name,
        "address": req.body.address,
        "features": req.body.features,
        "amenities": req.body.amenities,
        "landmarks": req.body.landmarks,
        "total_price": req.body.total_price,
        "monthly": req.body.monthly
    });

    newProperty.save(function (err, properties) {
        if (!err) {
            return res.json({
                "property": properties
            })
        } else {
            return res.send(err);
        }
    }); //save using the mongoose model
    // res.send("Fruit saved");
});


//============SHOW ALL FRUIT================//
router.get("/showAll", function (req, res) {
    // FruitModel.find({}, function (err, result) {
    // 	if(!err) {
    // 		return res.json({"result":result});
    // 	} else {
    // 		return res.send(err);
    // 	}
    // }); --- or ---
    PropertyModel.find({}).then(function (result) {
        return res.json({ "result": result });
    })
});

//============SHOW ONE FRUIT================//
router.get("/showOne/:id", function (req, res) {
    PropertyModel.find({ "_id": req.params.id }).then(function (result) {
        return res.json({ "result": result });
    });
});


//============DELETE A SINGLE FRUIT================//
router.delete("/delete/:id", function (req, res) {
    PropertyModel.deleteOne({ "_id": req.params.id }).then(function (result) {
        if (result) {
            return res.json({ "result": result });
        } else {
            return res.send("No item to delete.");
        }
    })
});

//============EDIT A SINGLE FRUIT================//
router.put("/update/:id", checkAdmin, function (req, res) {
    PropertyModel.updateOne({ "_id": req.params.id }, req.body).then(function (result) {
        if (result) {
            return res.json({ "result": result });
        } else {
            return res.send("No item to update.");
        }

    })
})



module.exports = router;
