const express = require("express");

const router = express.Router(); //use .Router if it is not the main app
const checkAuth = require('../middleware/check-auth');
const checkAdmin = require('../middleware/check-admin');
//get the Fruit model
const EventModel = require("../models/Event");


//============CREATE A NEW FRUIT================//
router.post("/create", function (req, res) {
    // let newFruit = FruitModel({
    // 	"name": "kiwi",
    // 	"color": "green",
    // 	"price": 20,
    // 	"taste": "sour"
    // });

    // res.send(req.body);

    let newEvent = EventModel({
        "date": req.body.date,
        "property": req.body.property

    });

    newEvent.save(function (err, events) {
        if (!err) {
            return res.json({
                "event": events
            })
        } else {
            return res.send(err);
        }
    }); //save using the mongoose model
    // res.send("Fruit saved");
});


//============SHOW ALL FRUIT================//
router.get("/showAll", function (req, res) {
    // FruitModel.find({}, function (err, result) {
    // 	if(!err) {
    // 		return res.json({"result":result});
    // 	} else {
    // 		return res.send(err);
    // 	}
    // }); --- or ---
    EventModel.find({}).populate('property').then(function (result) {
        return res.json({ "result": result });
    })
});

//============SHOW ONE FRUIT================//
router.get("/showOne/:id", function (req, res) {
    EventModel.find({ "_id": req.params.id }).populate('property').then(function (result) {
        return res.json({ "result": result });
    });
});
//============SHOW ONE FRUIT================//
router.get("/showForProperty/:id", function (req, res) {
    EventModel.find({ "property": req.params.id }).populate('property').then(function (result) {
        return res.json({ "result": result });
    });
});

//============DELETE A SINGLE FRUIT================//
router.delete("/delete/:id", function (req, res) {
    EventModel.deleteOne({ "_id": req.params.id }).then(function (result) {
        if (result) {
            return res.json({ "result": result });
        } else {
            return res.send("No item to delete.");
        }
    })
});

//============EDIT A SINGLE FRUIT================//
router.put("/update/:id", function (req, res) {
    EventModel.updateOne({ "_id": req.params.id }, req.body).then(function (result) {
        if (result) {
            return res.json({ "result": result });
        } else {
            return res.send("No item to update.");
        }

    })
})



module.exports = router;
