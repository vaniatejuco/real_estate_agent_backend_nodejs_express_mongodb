const express = require("express");

const router = express.Router(); //use .Router if it is not the main app

//get the Fruit model
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const checkAuth = require('../middleware/check-auth');
const checkAdmin = require('../middleware/check-admin');
const UserModel = require("../models/User");


//============CREATE A NEW FRUIT================//
router.post("/signup", function (req, res, next) {
    UserModel.find({email: req.body.email})
    .exec()
    .then(user => {
        if (user.length >= 1){
            return res.status(409).json({
                message: 'Mail exists'
            });
        } else {
            bcrypt.hash(req.body.password, 10, (err, hash) => {
                if (err) {
                    return res.status(500).json({
                        error:err
                    });
                } else {
                    let newUser = UserModel({
                       "name": req.body.name,
                        "email": req.body.email,
                        "contact_number": req.body.contact_number,
                        "password": hash
                    });
                    newUser.save()
                        .then(result => {
                            console.log(result);
                            res.status(201).json({
                                message: 'User created'
                            });
                         })
                    .catch(err => {
                        console.log(err);
                        res.status(500).json({
                            error: err
                        });
                    });
        
                }
            })

        }
    })


});


// ============LOGIN==================//

router.post('/login', (req, res, next) => {
    UserModel.find({ email: req.body.email})
    .exec()
    .then(user => {
        if (user.length < 1) {
            return res.status(401).json({
                message: 'Auth failed'
            });
        }
        bcrypt.compare(req.body.password, user[0].password, (err, result) => {
            if (err) {
                return res.status(401).json({
                    message: 'Auth failed'
                });
            }
            if (result) {
                const token = jwt.sign({
                    email: user[0].email,
                    userId: user[0]._id,
                    name: user[0].name,
                    role: user[0].role
                }, process.env.JWT_KEY || "secret",
                {
                   expiresIn: "1h" 
                });
                return res.status(200).json({
                    message: 'Auth successful',
                    email: user[0].email,
                    userId: user[0]._id,
                    name: user[0].name,
                    role: user[0].role,
                    token: token
                });
            }
            res.status(401).json({
                message: 'Auth failed'
            });
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});
 


//============SHOW ALL FRUIT================//
router.get("/showAll", function (req, res) {
    // FruitModel.find({}, function (err, result) {
    // 	if(!err) {
    // 		return res.json({"result":result});
    // 	} else {
    // 		return res.send(err);
    // 	}
    // }); --- or ---
    UserModel.find({}).then(function (result) {
        return res.json({ "result": result });
    })
});

//============SHOW ONE FRUIT================//
router.get("/showOne/:id", function (req, res) {
    UserModel.find({ "_id": req.params.id }).then(function (result) {
        return res.json({ "result": result });
    });
});

//============DELETE A SINGLE FRUIT================//
router.delete("/delete/:id", checkAdmin, function (req, res) {
    UserModel.deleteOne({ "_id": req.params.id }).then(function (result) {
        if (result) {
            return res.json({ "result": result });
        } else {
            return res.send("No item to delete.");
        }
    })
});

//============EDIT A SINGLE FRUIT================//
router.put("/update/:id", checkAdmin, function (req, res) {
    UserModel.updateOne({ "_id": req.params.id }, req.body).then(function (result) {
        if (result) {
            return res.json({ "result": result });
        } else {
            return res.send("No item to update.");
        }

    })
})



module.exports = router;
