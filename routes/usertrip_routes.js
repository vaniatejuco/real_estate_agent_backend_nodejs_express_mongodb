const express = require("express");

const router = express.Router(); //use .Router if it is not the main app
const checkAuth = require('../middleware/check-auth');
const checkAdmin = require('../middleware/check-admin');
//get the Fruit model
const UserTripModel = require("../models/UserTrip");


//============CREATE A NEW FRUIT================//
router.post("/create", function (req, res) {
    // let newFruit = FruitModel({
    // 	"name": "kiwi",
    // 	"color": "green",
    // 	"price": 20,
    // 	"taste": "sour"
    // });

    // res.send(req.body);

    let newUserTrip = UserTripModel({
        "event": req.body.event_id,
	"property": req.body.property_id,
        "user": req.body.user_id

    });

    newUserTrip.save(function (err, usertrips) {
        if (!err) {
            return res.json({
                "usertrip": usertrips
            })
        } else {
            return res.send(err);
        }
    }); //save using the mongoose model
    // res.send("Fruit saved");
});


//============SHOW ALL FRUIT================//
router.get("/showAll", function (req, res) {
    // FruitModel.find({}, function (err, result) {
    // 	if(!err) {
    // 		return res.json({"result":result});
    // 	} else {
    // 		return res.send(err);
    // 	}
    // }); --- or ---
    UserTripModel.find({}).populate('event').then(function (result) {
        return res.json({ "result": result });
    })
});

//============SHOW ONE FRUIT================//
router.get("/showOne/:id", function (req, res) {
    UserTripModel.find({ "_id": req.params.id }).populate('event').then(function (result) {
        return res.json({ "result": result });
    });
});
//============SHOW ONE FRUIT================//
router.get("/showByEvent/:id", function (req, res) {
    UserTripModel.find({ "event": req.params.id }).populate('event').then(function (result) {
        return res.json({ "result": result });
    });
});
//============SHOW ONE FRUIT================//
router.get("/showByStatus/:status", function (req, res) {
    UserTripModel.find({ "status": req.params.status }).populate('user').then(function (result) {
        return res.json({ "result": result });
    });
});
//============SHOW ONE FRUIT================//
router.get("/showByUser/:id", function (req, res) {
    UserTripModel.find({ "user": req.params.id }).populate('event').then(function (result) {
        return res.json({ "result": result });
    });
});

//============DELETE A SINGLE FRUIT================//
router.delete("/delete/:id", function (req, res) {
    UserTripModel.deleteOne({ "_id": req.params.id }).then(function (result) {
        if (result) {
            return res.json({ "result": result });
        } else {
            return res.send("No item to delete.");
        }
    })
});

//============EDIT A SINGLE FRUIT================//
router.put("/update/:id", function (req, res) {
    UserTripModel.updateOne({ "_id": req.params.id }, req.body).then(function (result) {
        if (result) {
            return res.json({ "result": result });
        } else {
            return res.send("No item to update.");
        }

    })
})



module.exports = router;
