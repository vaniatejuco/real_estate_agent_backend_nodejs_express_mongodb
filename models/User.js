//use mongoose to create a model for fruits collection
const mongoose = require("mongoose");

//load mongooose's Schema to prepare a template for Schema
const Schema = mongoose.Schema;

//Create a Schema for fruits
const UserSchema = new Schema({
    //name, color, price, taste
    name: String,
    email: {type: String, unique: true, match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/},
    contact_number: String,
    password: String,
    role: {type:Number, default:2}

});

//export the created schema as a model  for a main app to use
module.exports = mongoose.model("User", UserSchema); //Fruit = Fruit.js