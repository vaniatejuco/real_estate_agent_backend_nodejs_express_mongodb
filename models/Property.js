//use mongoose to create a model for fruits collection
const mongoose = require("mongoose"); 

//load mongooose's Schema to prepare a template for Schema
const Schema = mongoose.Schema;

//Create a Schema for fruits
const PropertySchema = new Schema({
    //name, color, price, taste
    name: String,
    address: String,
    features: String,
    amenities: String,
    landmarks: String,
    total_price: Number,
    monthly: Number,
});

//export the created schema as a model  for a main app to use
module.exports = mongoose.model("Property", PropertySchema); //Fruit = Fruit.js