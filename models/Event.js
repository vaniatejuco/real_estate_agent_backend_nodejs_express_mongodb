//use mongoose to create a model for fruits collection
const mongoose = require("mongoose"); 

//load mongooose's Schema to prepare a template for Schema
const Schema = mongoose.Schema;

//Create a Schema for fruits
const EventSchema = new Schema({
    //name, color, price, taste
    date: Date,
    property: {type: mongoose.Schema.Types.ObjectId, ref: 'Property', required: true}

});

//export the created schema as a model  for a main app to use
module.exports = mongoose.model("Event", EventSchema); //Fruit = Fruit.js