//use mongoose to create a model for fruits collection
const mongoose = require("mongoose");

//load mongooose's Schema to prepare a template for Schema
const Schema = mongoose.Schema;

//Create a Schema for fruits
const UserTripSchema = new Schema({
    //name, color, price, taste
    status: { type: String, default: "pending" },
    event: { type: mongoose.Schema.Types.ObjectId, ref: 'Event', required: true},
    property: { type: mongoose.Schema.Types.ObjectId, ref: 'Property', required: true},	
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true }

});

//export the created schema as a model  for a main app to use
module.exports = mongoose.model("UserTrip", UserTripSchema); //Fruit = Fruit.js
