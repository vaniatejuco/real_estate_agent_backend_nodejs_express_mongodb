const express = require("express"); //gets the package express

const mongoose = require("mongoose"); //Mongoose is for handling database connection for NoSQL

const bodyParser = require("body-parser"); //Body-parser allows us to parse/read JSON content in the body

const cors = require("cors"); // include the cors package/CORS allows for cross-origin requests

const app = express(); //use the package express to create a backend server

//To establish a connection to the local database
// mongoose.connect("mongodb://localhost/sampledb");

//To establish a connection to the MongoDB ATLAS
const databaseUrl = process.env.DATABASE_URL ||
    'mongodb+srv://admin:test1234@mycluster-ufzbj.mongodb.net/test?retryWrites=true';
//link in mongodb atlas

mongoose.connect(databaseUrl, { useNewUrlParser: true });

mongoose.connection.once("open", () => {
    console.log("Remote Database Connection Established");
});

app.use(bodyParser.json()); //allows bodyParser to read JSON files

app.use(cors());

const port = process.env.PORT || 3000; //turn off all other open nodemon

app.listen(port, () => {
    console.log(`Listening to port ${port}`)
});

app.get("/", function (req, res) {
    return res.json({ "message": "Hello World!" });
});

// app.get("/fruitBasket", function(req, res) {
// 	let fruitBasket = FruitModel({
// 		"name": req.body.name,
// 		"color": req.body.color,
// 		"subject1": req.body.subject1,
// 		"subject2": req.body.subject2,
// 		"subject3": req.body.subject3,
// 		"gwa": 0
// 	});

// 	return res.json({"name": "Hello World!"});
// });

// const fruit_routes = require("./routes/fruit_routes");
// app.use("/fruits", fruit_routes);

// const student_routes = require("./routes/student_routes");
// app.use("/students", student_routes);

const property_routes = require("./routes/property_routes.js");
app.use("/property", property_routes);

const event_routes = require("./routes/event_routes.js");
app.use("/event", event_routes);


const user_routes = require("./routes/user_routes.js");
app.use("/user", user_routes);

const usertrip_routes = require("./routes/usertrip_routes.js");
app.use("/usertrip", usertrip_routes);